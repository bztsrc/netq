/*
 * netq/client.c
 *
 * Copyright (C) 2022 bzt (bztsrc@gitlab) MIT license
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * @brief An example NetQ client for IPv4 and IPv6 UDP
 * https://gitlab.com/bztsrc/netq
 *
 * Compile with: gcc client.c -o client
 */

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <netdb.h>
#include <sys/socket.h>
#include <netinet/in.h>

#define NETQ_IMPLEMENTATION
#define NETQ_SEND my_sender
#include "netq.h"

/* the transport layer context */
typedef struct {
    int sock;
    struct sockaddr_storage peer_addr;
    socklen_t addrlen;
} my_net_t;
my_net_t my_net;

/* the raw packet sender */
int my_sender(void *net, const void *message, int length)
{
    my_net_t *my_net = (my_net_t *)net;
    return sendto(my_net->sock, message, length, 0, (struct sockaddr*)&my_net->peer_addr, my_net->addrlen);
}

/* the raw packet receiver */
int my_receiver(void *net, void *message, int length)
{
    my_net_t *my_net = (my_net_t *)net;
    my_net->addrlen = sizeof(struct sockaddr_storage); /* <- the recvfrom() API is tricky */
    return recvfrom(my_net->sock, message, length, 0, (struct sockaddr*)&my_net->peer_addr, &my_net->addrlen);
}

/* the client needs only one queue */
netq_t nq = { 0 };

/**
 * Example client
 */
int main(int argc, char **argv)
{
    int ret, n = 1;
    struct sockaddr_storage bind_addr;
    struct addrinfo hints, *cur, *addr_list = NULL;
    char buf[NETQ_MTU];

    if(argc < 3) {
        printf("%s <server ip> <port>\n", argv[0]);
        return 1;
    }

    /* get binary ip address for hostname / ip string, the usual stuff, nothing NetQ specific here */
    memset(&bind_addr, 0, sizeof(bind_addr));
    memset(&my_net, 0, sizeof(my_net));
    memset(&hints, 0, sizeof(hints));
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_DGRAM;
    hints.ai_protocol = IPPROTO_UDP;
    if(getaddrinfo(argv[1], argv[2], &hints, &addr_list) != 0) {
        fprintf(stderr, "getaddrinfo failed.\n"); return 1; }
    for(cur = addr_list; cur != NULL && !my_net.addrlen; cur = cur->ai_next) {
        my_net.sock = (int)socket(cur->ai_family, cur->ai_socktype, cur->ai_protocol); n = 1;
        if(my_net.sock < 0) continue;
        /* we don't care about our ip, any will do */
        memset(&bind_addr, 0, sizeof(bind_addr));
        bind_addr.ss_family = cur->ai_family;
        if(cur->ai_family == AF_INET) ((struct sockaddr_in*)&bind_addr)->sin_addr.s_addr = INADDR_ANY;
        else                          ((struct sockaddr_in6*)&bind_addr)->sin6_addr = in6addr_any;
        if(setsockopt(my_net.sock, SOL_SOCKET, SO_REUSEADDR, (const char *)&n, sizeof(n)) != 0 ||
           bind(my_net.sock, (struct sockaddr*)&bind_addr, cur->ai_addrlen) != 0) { close(my_net.sock); continue; }
        memcpy(&my_net.peer_addr, cur->ai_addr, cur->ai_addrlen);
        my_net.addrlen = cur->ai_addrlen;
    }
    freeaddrinfo(addr_list);

    /**************************************************
     * do some testing by sending / receiving packets *
     **************************************************/
    for(n = 1; n < 5; n++) {
        printf("-------------------------------------------------------------------------------\n");
        /* send message to server */
        sprintf(buf, "Hello %d", n);
        if((ret = netq_send(&nq, buf, strlen(buf) + 1, &my_net)) < 0)
            printf("netq_send returned %d\n", ret);
        netq_dump(&nq);

        /* get server's response */
        if((ret = my_receiver(&my_net, buf, sizeof(buf))) > 0) {
            netq_push(&nq, buf, ret, &my_net);
            netq_dump(&nq);
            netq_pop(&nq, buf, sizeof(buf));
            netq_dump(&nq);
        } else
            printf("recvfrom returned %d\n", ret);
    }

    close(my_net.sock);
    return 0;
}
