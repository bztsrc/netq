/*
 * netq/server.c
 *
 * Copyright (C) 2022 bzt (bztsrc@gitlab) MIT license
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * @brief An example NetQ server for multiple clients on IPv4 and IPv6 UDP
 * https://gitlab.com/bztsrc/netq
 *
 * Compile with: gcc server.c -o server
 */

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <netdb.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>  /* for inet_ntop */

#define NETQ_IMPLEMENTATION
#define NETQ_SEND my_sender
#include "netq.h"

/* the transport layer context */
typedef struct {
    int sock;
    struct sockaddr_storage peer_addr;
    socklen_t addrlen;
} my_net_t;
my_net_t my_net;

/* the raw packet sender */
int my_sender(void *net, const void *message, int length)
{
    my_net_t *my_net = (my_net_t *)net;
    return sendto(my_net->sock, message, length, 0, (struct sockaddr*)&my_net->peer_addr, my_net->addrlen);
}

/* the raw packet receiver (note: it's non-blocking for the server) */
int my_receiver(void *net, void *message, int length)
{
    my_net_t *my_net = (my_net_t *)net;
    my_net->addrlen = sizeof(struct sockaddr_storage); /* <- the recvfrom() API is tricky */
    return recvfrom(my_net->sock, message, length, MSG_DONTWAIT, (struct sockaddr*)&my_net->peer_addr, &my_net->addrlen);
}

/* the server needs multiple queues, one for each peer */
typedef struct {
    struct sockaddr peer_addr;
    netq_t nq;
    char lastmsg[NETQ_MTU];
} peer_t;
peer_t *peers = NULL;
int numpeers = 0;

/* return the context for a peer */
netq_t *my_getqueue(my_net_t *net)
{
    int i;

    /* look for matching source ip and port */
    for(i = 0; i < numpeers && memcmp(&peers[i].peer_addr, &net->peer_addr, sizeof(struct sockaddr)); i++);
    /* if not found, add a new record */
    if(i == numpeers) {
        peers = realloc(peers, ++numpeers * sizeof(peer_t));
        memset(&peers[i], 0, sizeof(peer_t));
        memcpy(&peers[i].peer_addr, &net->peer_addr, sizeof(struct sockaddr));
        /* should we have used pthreads */
        /* pthread_mutex_init(&peers[i].nq.mutex); */
    }
    return &peers[i].nq;
}

/* display peers in a nice ASCII table */
void peers_dump()
{
    char str[INET6_ADDRSTRLEN];
    int i;

    printf("\x1b[H\x1b[2J"
           "NetQ configuration: sequence number: uint%u_t, ack mask: uint%u_t, mtu: %u, header: %u, queue: %u, memory: %u bytes"
#ifdef NETQ_MUTEX_TYPE
        ", thread-safety"
#endif
        ".\n", NETQ_SEQ_BITS, NETQ_ACK_BITS, NETQ_MTU, (unsigned int)sizeof(netq_hdr_t), NETQ_QUEUE_SIZE, (unsigned int)sizeof(netq_t));

    printf(  "\n| Peer | Source IP and Port         | Out | In  | Ack | Pop | Last Message   |\n"
               "|------|----------------------------|-----|-----|-----|-----|----------------|\n");
    for(i = 0; i < numpeers; i++) {
        inet_ntop(peers[i].peer_addr.sa_family, peers[i].peer_addr.sa_family == AF_INET ?
          (void*)&((struct sockaddr_in*)&peers[i].peer_addr)->sin_addr :
          (void*)&((struct sockaddr_in6*)&peers[i].peer_addr)->sin6_addr, str, sizeof(str));
        printf("|%5u ""| %-20s %5u "               "| %3u | %3u | %3u | %3u | %-15s"        "|\n",
            i, str, ntohs(peers[i].peer_addr.sa_family == AF_INET ?
            ((struct sockaddr_in*)&peers[i].peer_addr)->sin_port : ((struct sockaddr_in6*)&peers[i].peer_addr)->sin6_port),
            peers[i].nq.seq_out, peers[i].nq.seq_in, peers[i].nq.seq_ack, peers[i].nq.seq_pop, peers[i].lastmsg
        );
    }
    printf("\n");
}

/**
 * Example server
 */
int main(int argc, char **argv)
{
    int i, ret, n = 1;
    struct sockaddr_storage bind_addr;
    struct addrinfo hints, *cur, *addr_list = NULL;
    char buf[NETQ_MTU];
    netq_t *nq;

    if(argc < 3) {
        printf("%s <bind ip> <port> [--peer-list]\n", argv[0]);
        return 1;
    }

    /* get binary ip address for hostname / ip string, the usual stuff, nothing NetQ specific here */
    memset(&bind_addr, 0, sizeof(bind_addr));
    memset(&my_net, 0, sizeof(my_net));
    memset(&hints, 0, sizeof(hints));
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_DGRAM;
    hints.ai_protocol = IPPROTO_UDP;
    if(getaddrinfo(argv[1], argv[2], &hints, &addr_list) != 0) {
        fprintf(stderr, "getaddrinfo failed.\n"); return 1; }
    for(cur = addr_list; cur != NULL && !my_net.addrlen; cur = cur->ai_next) {
        my_net.sock = (int)socket(cur->ai_family, cur->ai_socktype, cur->ai_protocol); n = 1;
        if(my_net.sock < 0) continue;
        if(setsockopt(my_net.sock, SOL_SOCKET, SO_REUSEADDR, (const char *)&n, sizeof(n)) != 0 ||
           bind(my_net.sock, cur->ai_addr, cur->ai_addrlen) != 0) { close(my_net.sock); continue; }
        memcpy(&bind_addr, cur->ai_addr, cur->ai_addrlen);
        my_net.addrlen = cur->ai_addrlen;
    }
    freeaddrinfo(addr_list);

    /* print out what we've got */
    inet_ntop(bind_addr.ss_family, bind_addr.ss_family == AF_INET ?
      (void*)&((struct sockaddr_in*)&bind_addr)->sin_addr : (void*)&((struct sockaddr_in6*)&bind_addr)->sin6_addr,
      buf, sizeof(buf));
    printf("waiting for %s connections on %s port %u\n", bind_addr.ss_family == AF_INET ? "IPv4" : "IPv6", buf,
        ntohs(bind_addr.ss_family == AF_INET ? ((struct sockaddr_in*)&bind_addr)->sin_port : ((struct sockaddr_in6*)&bind_addr)->sin6_port));

    /**************************************************
     * the main server loop                           *
     **************************************************/
    while(1) {
        /* push the received raw packet to the peer's queue */
        if((ret = my_receiver(&my_net, buf, sizeof(buf))) > 0) {
            if(!argv[3]) printf("-------------------------------------------------------------------------------\n");
            nq = my_getqueue(&my_net);
            netq_push(nq, buf, ret, &my_net);
            if(!argv[3]) netq_dump(nq);
        }

        /* iterate on all network queues, and see if any has a message to be processed */
        for(i = 0; i < numpeers; i++)
            if((ret = netq_pop(&peers[i].nq, peers[i].lastmsg, sizeof(peers[i].lastmsg))) > 0) {
                /* process your message in buf here */
                printf("peer #%d has sent %d bytes: '%s'\n", i, ret, peers[i].lastmsg);
                /* send reply, echo message back */
                netq_send(&peers[i].nq, buf, strlen(buf) + 1, &my_net);
                /* dump queues */
                if(!argv[3]) netq_dump(&peers[i].nq);
                else peers_dump();
            }
    }

    close(my_net.sock);
    return 0;
}
